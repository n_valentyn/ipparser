<?php


namespace AdditionComponentsNVM\IpParser;


class MaxMindAdapter implements AdapterInterface
{
    protected $reader;
    protected $record;

    public function __construct(\GeoIp2\Database\Reader $reader)
    {
        $this->reader = $reader;
    }

    public function parse(string $ip)
    {
        try {
            $this->record = $this->reader->city($ip);
        } catch (\GeoIp2\Exception\AddressNotFoundException $exception) {
            return false;
        }
        return true;
    }

    public function getCountryCode()
    {
        return $this->record->country->isoCode;
    }

    public function getCountryName()
    {
        return $this->record->country->name;
    }

    public function getCityName()
    {
        return $this->record->city->name;
    }

}
