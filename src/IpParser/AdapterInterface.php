<?php


namespace AdditionComponentsNVM\IpParser;


interface AdapterInterface
{
    public function parse(string $ip);

    public function getCountryCode();

    public function getCountryName();

    public function getCityName();
}
