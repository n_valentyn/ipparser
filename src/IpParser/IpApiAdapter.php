<?php

namespace AdditionComponentsNVM\IpParser;

class IpApiAdapter implements AdapterInterface
{
    protected $data;

    public function parse(string $ip)
    {
        $result = file_get_contents('http://ip-api.com/json/' . $ip);
        $this->data = json_decode($result, true);

        if($this->data['status'] == 'fail') {
            return false;
        }
        return true;
    }

    public function getCountryCode()
    {
        return $this->data['countryCode'];
    }

    public function getCountryName()
    {
        return $this->data['country'];
    }

    public function getCityName()
    {
        return $this->data['city'];
    }
}
