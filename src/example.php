<?php
require_once '../vendor/autoload.php';

//$reader = new \GeoIp2\Database\Reader('resources/GeoLite2/GeoLite2-City.mmdb');
//$ipParser =  new \AdditionComponentsNVM\IpParser\MaxMindAdapter($reader);

$ipParser =  new \AdditionComponentsNVM\IpParser\IpApiAdapter();

echo '<pre>' . PHP_EOL;

echo $ipParser->parse('37.73.196.83') . PHP_EOL;

echo $ipParser->getCityName() . PHP_EOL;

echo $ipParser->getCountryName() . PHP_EOL;

echo $ipParser->getCountryCode() . PHP_EOL;